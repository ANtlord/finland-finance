import sys

from decimal import Decimal
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from fifi import models
from fifi import fields


User = get_user_model()


class Command(BaseCommand):
    help = 'Load some money to the pointed account'

    def add_arguments(self, parser):
        parser.add_argument('card_id', type=str)
        parser.add_argument('amount', type=Decimal)
        parser.add_argument(
            'currency', choices=[x for x, _ in fields.Currency.CURRENCIES]
        )

    def handle(self, *args, **kwargs):
        amount = kwargs['amount']
        currency = kwargs['currency']
        card_id = kwargs['card_id']
        account = models.Account.objects.filter(id=card_id).first()
        if not account:
            self.stdout.write(self.style.ERROR(f'There is not account with card id {card_id}'))
            sys.exit(-1)
        if account.currency != currency:
            self.stdout.write(self.style.ERROR(
                f'Account {card_id} supports currency {account.currency}'
            ))
            sys.exit(-1)
        models.CashTransfer.objects.create(account=account, amount=amount)
        self.stdout.write(self.style.SUCCESS(f'{currency} {amount} loaded succesfully'))
