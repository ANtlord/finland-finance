from django.contrib import admin
from django.urls import path
from django.urls import include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(('fifi.api.urls', 'api'), namespace='api')),
    path('scheme-api/', include(('fifi.scheme_api.urls', 'scheme_api'), namespace='scheme-api')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

admin.site.site_header = 'Finance Finland'
