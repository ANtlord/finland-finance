from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from fifi import models


@admin.register(models.User)
class UserAdmin(BaseUserAdmin):
    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.Account)
class AccountAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False


class _ReadOnlyAdminMixin():
    """Disables all editing capabilities."""
    def get_actions(self, request):
        actions = super().get_actions(request)
        del actions["delete_selected"]
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        pass

    def delete_model(self, request, obj):
        pass

    def save_related(self, request, form, formsets, change):
        pass

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super().changeform_view(request, object_id, form_url, extra_context)

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


@admin.register(models.Transaction)
class TransactionAdmin(_ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


@admin.register(models.Transfer)
class TransferAdmin(_ReadOnlyAdminMixin, admin.ModelAdmin):
    pass
