from django.urls import path
from fifi.scheme_api import transactions

urlpatterns = [
    path('transactions/', transactions.CreateAPIView.as_view(), name='transaction-list'),
]
