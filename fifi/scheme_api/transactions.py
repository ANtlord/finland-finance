from rest_framework import status
from rest_framework import serializers
from rest_framework import generics
from rest_framework import exceptions
from rest_framework.response import Response

from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from fifi import models

User = get_user_model()
_TRANSACTION_STATES = models.Transaction.STATES
_TRANSACTION_STATES_DICT = {x: y for x, y in _TRANSACTION_STATES}


class _ChoiceField(serializers.ChoiceField):
    def to_internal_value(self, data):
        for key, value in self.choices.items():
            if value == data:
                return key
        self.fail('invalid_choice', input=data)


class _AuthorizeSerializer(serializers.ModelSerializer):
    """Serializer creates transaction and authorize it."""
    type = _ChoiceField(source='state', choices=(_TRANSACTION_STATES[0],))
    transaction_id = serializers.CharField(source='id')
    transaction_amount = serializers.DecimalField(
        source='amount',
        max_digits=models.Transaction.get_field('amount').max_digits,
        decimal_places=models.Transaction.get_field('amount').decimal_places,
    )
    transaction_currency = serializers.ChoiceField(
        source='currency', choices=models.Transaction.get_field('currency').choices
    )
    card_id = serializers.PrimaryKeyRelatedField(
        source='account', read_only=False, queryset=models.Account.objects.all()
    )

    class Meta:
        model = models.Transaction
        fields = (
            'type',
            'card_id',
            'transaction_id',
            'merchant_name',
            'merchant_country',
            'merchant_city',
            'merchant_mcc',
            'billing_amount',
            'billing_currency',
            'transaction_amount',
            'transaction_currency',
        )

    def validate(self, data):
        """Check there is enough money to pay the transaction."""
        account = data['account']
        if account.available_balance < data['billing_amount']:
            raise serializers.ValidationError({'billing_amount': _('Not enough minerals')})
        return data

    def create(self, validated_data):
        """Create a related transfer to hold money at related account.

        TEST ASSIGNMENT NOTE. Transfer creation should be done in celery task to shrink waiting time
        of Scheme.
        """
        instance = super().create(validated_data)
        models.TransactionTransfer.create_for_transaction(instance)
        return instance


class _PresentSerializer(serializers.ModelSerializer):
    """Serializer presents existed transaction."""
    type = _ChoiceField(source='state', choices=(models.Transaction.STATES[1],))

    class Meta:
        model = models.Transaction
        fields = (
            'type',
            'billing_amount',
            'settlement_amount',
            'settlement_currency',
        )

    def _pay_revenue(self, transaction, validated_data):
        """Send revenue to a bank account. Revenue amount is a difference
        between settlement amount and billing amount.

        :param transaction: a transaction the revenue transfer will be related.
        :param validated_data: data provides settlement and billing amounts.
        """
        settlement_amount = validated_data['settlement_amount']
        billing_amount = validated_data['billing_amount']
        user = User.objects.get(username=settings.ISSUER_USER)
        issuer_account = user.accounts.get(currency=transaction.billing_currency)
        revenue = billing_amount - settlement_amount
        models.TransactionTransfer.objects.create(
            account=issuer_account, amount=revenue, transaction=transaction
        )

    def update(self, instance, validated_data):
        """Update transfer money. Create revenue.

        TEST ASSIGNMENT NOTE. Transfer mutation should be done in celery task to shrink waiting time
        of Scheme.
        """
        transfer = models.TransactionTransfer.objects.get(
            account=instance.account, transaction=instance.pk,
        )
        transfer.amount = -validated_data['billing_amount']
        transfer.save()
        self._pay_revenue(instance, validated_data)
        return super().update(instance, validated_data)


class CreateAPIView(generics.CreateAPIView):
    """Endpoint supports Scheme webhooks by authorization and presentment of a specified
    transaction.

    The endpoint accepts data with `type: "authorization"` creating new transaction.
    The endpoint accepts data with `type: "presetment"` updating authorized transaction.
    """
    queryset = models.Transaction.objects.all()

    def create(self, request, *args, **kwargs):
        """Override status code due to Scheme accept only HTTP 200 code."""
        response = super().create(request, *args, **kwargs)
        response.status_code = status.HTTP_200_OK
        response.data = None
        return response

    def get_object(self):
        """Return authorized transaction.

        The transaction is looked up by id specified in 'transaction_id' field.
        If the transaction is not found it raises ValidationError due Scheme accepts
        only HTTP 403 code.

        TEST ASSIGNMENT NOTE. In a case like that I return 409 raising custom exception.
        The exception would be handled by my custom handler, pointed in EXCEPTION_HANDLER.
        I outrage Liskov principle omitting `lookup_expr` but the method could be reworked
        only with main logic of the ViewSet.

        In case of object that doesn't exist I return 404 instead of 403.
        """
        transaction_id = self.request.data.get('transaction_id')
        if not transaction_id:
            raise exceptions.ValidationError
        instance = self.get_queryset().authorized().filter(id=transaction_id).first()
        if not instance:
            raise exceptions.ValidationError
        return instance

    def get_serializer(self, *args, **kwargs):
        """Return an appropriate serializer for authorization or for presentment.

        TEST ASSIGNMENT NOTE. As Scheme sends only one type of request (I suppose it's POST) so
        I redefine the method due it is more appropriate method due to it is responsible for
        building a serializer so I don't outrage Liskov principle.
        """
        request_type = self.request.data.get('type')
        instance = None
        if request_type == _TRANSACTION_STATES_DICT[models.Transaction.PRESENTMENT]:
            instance = self.get_object()

        if instance:
            return _PresentSerializer(instance, *args, **kwargs)
        else:
            return _AuthorizeSerializer(*args, **kwargs)
