"""
Provide api for a cardholder allows to get transactions.
"""
from django_filters import rest_framework as filters

from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import generics

from fifi import models


class _FilterSet(filters.FilterSet):
    """Allows filtering transactions for a specific timeframe."""
    created = filters.DateTimeFromToRangeFilter()

    class Meta:
        model = models.Transaction
        fields = 'created',


class _Serializer(serializers.ModelSerializer):
    """Serializes a transaction."""
    billing_currency = serializers.ReadOnlyField()
    settlement_currency = serializers.ReadOnlyField()

    class Meta:
        model = models.Transaction
        fields = '__all__'


class ViewSet(viewsets.GenericViewSet, generics.ListAPIView):
    """Endpoint shows transactions of current user."""
    queryset = models.Transaction.objects.presented()
    serializer_class = _Serializer
    filter_class = _FilterSet

    def get_queryset(self):
        return super().get_queryset().for_user(self.request.user.pk)
