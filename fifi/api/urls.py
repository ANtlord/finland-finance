from rest_framework import routers
from fifi.api import transactions
from fifi.api import accounts

router = routers.DefaultRouter()

router.register('transactions', transactions.ViewSet)
router.register('accounts', accounts.ViewSet)

urlpatterns = router.urls
