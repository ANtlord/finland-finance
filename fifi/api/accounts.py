"""
Provide api for a cardholder allows getting balances of his/her accounts.
"""
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import generics

from fifi import models


class _Serializer(serializers.ModelSerializer):
    """Represent an Account instance."""
    ledger_balance = serializers.ReadOnlyField()
    available_balance = serializers.ReadOnlyField()

    class Meta:
        model = models.Account
        fields = '__all__'


class ViewSet(viewsets.GenericViewSet, generics.ListAPIView):
    """Endpoint provides Account of current user."""
    queryset = models.Account.objects.all()
    serializer_class = _Serializer

    def get_queryset(self):
        return super().get_queryset().for_user(self.request.user.pk)
