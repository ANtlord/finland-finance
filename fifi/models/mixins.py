from django.db import models


class TimestampMixin(models.Model):
    """Automatically adds time of creating and updating.

    :param created: time that is filled automatically when an instance is created.
    :param updated: time that is filled automatically when an instance is updated.
    """
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True
