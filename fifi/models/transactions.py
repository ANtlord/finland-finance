from django.utils.translation import ugettext_lazy as _
from django.db import models

from fifi import fields
from .accounts import Account
from .mixins import TimestampMixin


class _TransactionQueryset(models.QuerySet):
    def for_user(self, user_id: int):
        return self.filter(account__user_id=user_id)

    def presented(self):
        return self.filter(state=Transaction.PRESENTMENT)

    def authorized(self):
        return self.filter(state=Transaction.AUTHORIZATION)


class Transaction(TimestampMixin):
    """Represent a transaction.

    A transaction bundles a number of transfers. For example it could have two transfers.
    The first one is a transfer holds money at a cardholder's account. The second one is
    tranfer pays revenue the Issuer.

    :param id: unique character code of a transaction.
    :param account: account the transaction happens for.
    :param state: current state of the transaction.
    :param amount: transaction amount of the price the a user of the account pay for.
    :param currency: transaction amount of the price the a user of the account pay for.
    :param billing_amount: amount the transaction get after exchanging between currencies.
    :param settlement_amount: amount of money the bank transfers between Scheme, Issuer and Acquier.
    :param merchant_name: Name of the merchant that initiate authorization of the transaction.
    :param merchant_country: Country of the merchant.
    :param merchant_city: City of the merchant.
    :param merchant_mcc: The merchant catergory code.
    """
    AUTHORIZATION = 'arn'
    PRESENTMENT = 'pnt'

    STATES = (
        (AUTHORIZATION, _('authorisation')),
        (PRESENTMENT, _('presentment')),
    )

    objects = models.Manager.from_queryset(_TransactionQueryset)()

    id = models.CharField(max_length=9, primary_key=True)
    account = models.ForeignKey(Account, related_name='transactions', on_delete=models.PROTECT)
    state = models.CharField(max_length=3, blank=False, choices=STATES)

    amount = fields.build_positive_amount_field()
    currency = fields.Currency()
    billing_amount = fields.build_positive_amount_field()
    settlement_amount = fields.build_positive_amount_field(blank=True, null=True)

    merchant_name = models.CharField(max_length=255, blank=False)
    merchant_country = models.CharField(max_length=127, blank=False)
    merchant_city = models.CharField(max_length=127, blank=False)
    merchant_mcc = models.CharField(max_length=4, blank=False)

    class Meta:
        ordering = '-created',
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')

    @classmethod
    def get_field(cls, name):
        """Return parameters of a field that have specified name."""
        return [x for x in cls._meta.fields if x.name == name][0]

    def __str__(self):
        return f'{self.id} {self.amount} {self.currency} {self.account} -> {self.merchant_name}'

    def _validate_presentment(self):
        """Validate an authorized transaction."""
        error_tpl = 'Settlement %s is required for presented transaction'
        if self.state == self.PRESENTMENT:
            assert self.settlement_amount, error_tpl % 'amount'
            assert self.settlement_currency, error_tpl % 'currency'

    def _validate_authorized(self):
        """Validate an presented transaction."""
        error_tpl = 'Settlement %s must be NULL for authorized transaction'
        if self.state == self.AUTHORIZATION:
            assert not self.settlement_amount, error_tpl % 'amount'
            assert not self.settlement_currency, error_tpl % 'currency'

    def save(self, *args, **kwargs):
        """A transaction can be in 1 of 2 states. It should validated consider the state.

        TEST ASSIGNMENT NOTE. If there would more than 2 states and logic of
        switching between them would more complex I prefer to use pettern
        called State so validation related to state would moved to class
        represented the state.
        """
        self._validate_authorized()
        self._validate_presentment()
        return super().save(*args, **kwargs)

    @property
    def is_authorized(self):
        """Return True if a transaction is authorized."""
        return self.state == self.AUTHORIZATION

    @property
    def is_presented(self):
        """Return True if a transaction is presented."""
        return self.state == self.PRESENTMENT

    @property
    def billing_currency(self):
        """Return billing currency.

        TEST ASSIGNMENT NOTE. I doubt about it but consider the exercise billing currency of
        transaction is the same as currency of the account the transaction is for.
        """
        return self.account.currency

    @property
    def settlement_currency(self):
        """Return settlement currency.

        TEST ASSIGNMENT NOTE. I doubt about it but consider the exercise settlement currency of
        transaction is the same as currency of the account the transaction is for.
        """
        return self.account.currency if self.settlement_amount else None

    def is_deletable(self):
        """Can delete only authorized transaction.

        TEST ASSIGNMENT NOTE. I suppose that an authorized transaction has a some sort of timeout.
        When the timeout is through the transaction is deleted and holded amount of money is free.
        """
        return self.is_authorized

    def delete(self, *args, **kwargs):
        if self.is_deletable:
            super().delete(*args, **kwargs)
        raise NotImplementedError('Action is forbidden.')
