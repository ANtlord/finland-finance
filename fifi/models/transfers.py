from django.db.models import Q
from django.db import models
from django.utils.translation import ugettext_lazy as _

from fifi import fields
from .accounts import Account
from .transactions import Transaction
from .mixins import TimestampMixin


class _TransferQuerySet(models.QuerySet):
    def sum(self):
        """Return sum of amounts of selected transfers of there is no selected return 0."""
        if self:
            return self.aggregate(models.Sum('amount'))['amount__sum']
        return 0

    def for_ledger_balance(self):
        """Return transactions that appropriate to get ledger balance of related account."""
        return self.filter(
            Q(kind=Transfer.CASH) |
            Q(kind=Transfer.TRANSACTION, transaction__state=Transaction.PRESENTMENT)
        )


class Transfer(TimestampMixin):
    """Represent a transfer.

    The class must be used only for getting transactions of all kinds. To create a transfer use
    proxy models.

    :param kind: indicates a kind of a transaction.
    :param account: an account the transfer belongs to.
    :param amount: how money a transfer about.
    :param transaction: if kind the transaction have is 'tsn' then the transaction must be related
    to a transaction.
    """
    CASH = 'csh'
    TRANSACTION = 'tsn'
    KINDS = (
        (CASH, _('Cash')),
        (TRANSACTION, _('Transaction')),
    )

    objects = models.Manager.from_queryset(_TransferQuerySet)()

    kind = models.CharField(max_length=3, choices=KINDS)
    account = models.ForeignKey(Account, related_name='transfers', on_delete=models.PROTECT)
    amount = fields.build_amount_field()
    transaction = models.ForeignKey(
        Transaction, related_name='transfers', null=True, on_delete=models.PROTECT
    )

    class Meta:
        verbose_name = _('Transfer')
        verbose_name_plural = _('Transfers')

    @property
    def currency(self):
        """Currency of a transfer must be the same as currency of related account."""
        return self.account.currency

    def save(self, *args, **kwargs):
        """Saving is forbidden using base model. Use proxy models."""
        raise NotImplementedError

    def delete(self):
        """Deletion is forbidden using base model. Use proxy models."""
        raise NotImplementedError

    def __str__(self):
        return f'{self.amount} {self.currency} {self.account}'


class _CashTransferManager(models.Manager.from_queryset(_TransferQuerySet)):
    """Selects only cash transfers."""
    def get_queryset(self):
        return super().get_queryset().filter(kind=Transfer.CASH)


class CashTransfer(Transfer):
    """Represents a cash transfer. A cash transfer is not deletable or editable."""
    objects = _CashTransferManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.kind = Transfer.CASH
        if self.transaction:
            raise NotImplementedError('Cash transfer must not be related to a transaction')
        if self.pk:
            raise NotImplementedError('Can\'t edit a transfer')
        super(Transfer, self).save(*args, **kwargs)


class _TransactionTransferQuerySet(_TransferQuerySet):
    def authorized(self):
        """Return transfers of authorized transactions."""
        return self.filter(transaction__state=Transaction.AUTHORIZATION)

    def presented(self):
        """Return transfers of presented transactions."""
        return self.filter(transaction__state=Transaction.PRESENTMENT)


class _TransactionTransferManager(models.Manager.from_queryset(_TransactionTransferQuerySet)):
    """Selects only transaction transfers."""
    def get_queryset(self):
        return super().get_queryset().filter(kind=Transfer.TRANSACTION)


class TransactionTransfer(Transfer):
    """Represents a transaction transfer.

    A transaction can be removed only within related transaction and the transaction must have
    authorized state. The transfer is deletable or editable until the related transaction is
    presented.
    """
    objects = _TransactionTransferManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.kind = Transfer.TRANSACTION
        if not self.transaction:
            raise NotImplementedError('Transaction transfer must be related transaction')
        if self.pk and self.transaction.is_presented:
            raise NotImplementedError('Can\'t edit a transfer of presented transaction')
        super(Transfer, self).save(*args, **kwargs)

    @property
    def is_deletable(self):
        """If related transaction can be deletable then we can delete transfer."""
        return self.transaction and self.transaction.is_deletable

    def delete(self, *args, **kwargs):
        if self.is_deletable:
            super(Transfer, self).delete(*args, **kwargs)
        super().delete(*args, **kwargs)

    @classmethod
    def create_for_transaction(cls, transaction: Transaction):
        """Create a transfer for pointed transaction as a withdraw of an account the transaction
        belongs to.
        """
        assert transaction.is_authorized, 'New transfer must be only for authorized transaction'
        return cls.objects.create(
            transaction=transaction,
            amount=-transaction.billing_amount,
            account=transaction.account,
        )
