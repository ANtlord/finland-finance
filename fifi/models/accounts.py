from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractUser

from fifi import fields
from .mixins import TimestampMixin


class User(AbstractUser):
    """TEST ASSIGNMENT NOTE. I decided to redefine User model just in case."""
    pass


class _AccountQueryset(models.QuerySet):
    def for_user(self, user_id: int):
        """Return accounts for a user that is specified by id."""
        return self.filter(user_id=user_id)


class Account(TimestampMixin):
    """Represent an account of a user.

    :param id: arbitrary identifier that is used when a scheme authorizes a transaction.
    :param user: a user the account belongs to.
    :param currency: currency of money that will be billed by related transactions.
    """
    objects = models.Manager.from_queryset(_AccountQueryset)()

    id = models.CharField(max_length=8, blank=False, unique=True, primary_key=True)
    user = models.ForeignKey(User, related_name='accounts', on_delete=models.CASCADE)
    currency = fields.Currency()

    class Meta:
        verbose_name = _('Account')
        verbose_name_plural = _('Accounts')
        ordering = 'created',

    def __str__(self):
        return f'{self.user.get_full_name()} {self.card_id} {self.currency}'

    @property
    def card_id(self):
        """Alias used by a scheme."""
        return self.pk

    @property
    def ledger_balance(self):
        """Represents balance consider presented transactions."""
        return self.transfers.for_ledger_balance().sum()

    @property
    def available_balance(self):
        """Represents balance consider all transactions."""
        return self.transfers.sum()
