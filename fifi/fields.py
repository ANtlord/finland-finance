from django.utils.translation import ugettext_lazy as _
from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models


def build_amount_field(**kwargs):
    """Shortcut to create a decimal field that is appropriate for an amount."""
    return models.DecimalField(max_digits=10, decimal_places=2, **kwargs)


def build_positive_amount_field(**kwargs):
    """Shortcut to create a decimal field that is appropriate for a positive amount."""
    return build_amount_field(
        validators=[MinValueValidator(Decimal('0.01'))], **kwargs
    )


class Currency(models.CharField):
    """Represents currency in a model."""
    USD = 'USD'
    EUR = 'EUR'
    CAD = 'CAD'

    CURRENCIES = (
        (USD, _('US Dollar')),
        (EUR, _('Euro')),
        (CAD, _('Canadian Dollar')),
    )

    def __init__(self, **kwargs):
        kwargs.pop('max_length', None)
        kwargs.pop('choices', None)
        super().__init__(max_length=3, choices=self.CURRENCIES, **kwargs)
