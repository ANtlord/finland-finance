from setuptools import setup


setup(
    name='Finance Finland',
    version='0.0.1',
    packages=['fifi'],
    description='Test assignment implement Issuer.',
    setup_requires=['pytest-runner'],
    install_requires=[
        'Django==2.0',
        'djangorestframework==3.8.2',
        'django-filter==2.0.0.dev1',
    ],
    tests_require=[
        'pytest-django==3.2.1',
        'pytest',
        'factory_boy',
    ],
    include_package_data=True,
)
