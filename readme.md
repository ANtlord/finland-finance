# Finance Finland

Markov Alex's solution for a test exercise for Holvi. The solution is an fintech system works as Issuer providing
API for a cardholder and for a scheme.

## Requirements

* Python 3.6
* PIP (Tested on 10.0.1)

## Installation

Highly recommended to use virtual environment https://virtualenv.pypa.io/en/stable/. Also, in
order to authorize and present a transaction ensure you have `curl` https://curl.haxx.se/

* `git clone https://bitbucket.org/ANtlord/finland-finance.git`
* `cd finland-finance`
* `virtualenv .venv`
* `source .venv/bin/activate`
* `pip install -e .`
* `./manage.py loaddata bank_accounts_and_user.json`

It creates superuser, his login is `admin`, password is `admin`. Also, it creates 3 accounts for the
Issuer to receive revenues.

## Running

* `./manage.py runserver`
* Go to Admin panel and create some users and related accounts `http://localhost:8000/admin`
* Load some cash on specified account. `./manage.py load_money CARD0000 111 EUR`
* API for a user is `http://localhost:8000/api/`
* API for a scheme is `http://localhost:8000/scheme-api/`

## Tests

Run tests via `python setup.py pytest`. All tests are in `tests` module. The tests are
implemented with PyTest framework.

## Project architecture description

The solution consists of one application `fifi`. There are several modules:

* `api` - API for a cardholder allows getting balances and transactions.
* `scheme_api` - API for a scheme allows authorize and present a transaction.
* `models` - entities represents database tables.
* `management` - module contains only one command that allows loading money for
  a pointed account of someone cardholder.
* `admin` - provides required classes to build administration panel.
* `fields` - provides shortcuts and one custom field for currency.
* `settings` - module builds settings for the application.
* `urls` - module builds urls for the application.

Modules `api`, `scheme_api`, `models` are described further.

### API

The module consists of two modules `accounts` and `transactions`.

* `accounts` - provides an endpoint to retrieve data of accounts of a cardholder.
* `transactions` - provides an endpoint to retrieve data of transactions of a cardholder.

### SCHEME API

* `transactions` - provides an endpoint to authorize and present transactions. Technically authorization
is creation of a transaction that have state `authorized`. Also, presentation is updating of a
transaction. The updating includes setting state `presented` and setting of `settlement_amount`. So
both of actions are provided by only one endpoint. Choosing of action is based on data that is in POST data
of a request. The clue field of the data is `type`. If the field `type` is equal `authorization` then
the endpoint creates a transaction. If the field `type` is equal `presentment` then the endpoint finds
a transaction by value from field `transaction_id` and updates it.

### MODELS

There are several models:

* User - the model represents user. There is no complex logic. It's redefined just in case.
* Account - account is related to User model. It represents a card of a cardholder.
* Transaction - an entity logs for what a cardholder spent his/her money and bundles all related
transfers.
* Transfer - represents money moving. Base model that it's not for direct usage.
* CashTransfer - proxy model of Transfer. Represents money moving from cash to card balance.
* TransactionTransfer - proxy model of Transfer. Represents money moving
  related to transaction. For example: revenue of a merchant a transaction for,
  revenue of the Issuer.

## How it works

In order give a logic of that how it works I describe a use case consists of next steps:

* Working as an admin.
    * Creating a user.
    * Creating an account for the user.
    * Add some money for the account.
    * Checking the money you loaded.

* Working as a scheme.
    * Authorization of transaction for the account.
    * Presentment of the transaction.

* Working as the created user.
    * Viewing the transaction.
    * Viewing the account balance.

* Again working as an admin.
    * Viewing revenue.

**NOTICE:** Before going further please ensure you complete all steps from [Installation part](#Installation).

### Working as an admin

In order to create a user log in the administration panel with the username is `admin` and the
password is `admin`. Go to Users section, push Add User button and you get form for user creation so
fill the form. After saving the form you get extended form allows editing user data. So you can fill
First Name and Last Name fields for further convenience.

Next, go to Accounts section, it is available on the root of the administration panel. If you load initial data that is
in `bank_accounts_and_user.json` file you will get 3 accounts:

* BANK0002 CAD
* BANK0001 USD
* BANK0000 EUR

the accounts allows receiving revenue by the Issuer. Push Add Account button and you get 3 fields:

* Id - is a string identifier of the account. Type any 8 characters that will
  answer the question: "What the account is it?"
* User - a user the account will belong to. Select the user you just created.
* Currency - currency of money the account will store. Select currency that is appropriate for the
  created user.

Push Save button so you get fourth account in the list of the accounts.

In order to load some money for the account you just created there is a CLI command allows doing
that. Open your favorite shell, go to the project directory, activate virtual environment if you use
it. The command is called `load_money` and it accept 3 parameters:

* Account id. The value you typed in Id field of the Account creation form.
* Amount. How many money you want to load.
* Currency. The currency you selected in Currency field of the Account creation form.

If you created an account specifying `Id` equals to `CARD0000` and selecting EUR then you can type
`./manage.py load_money CARD0000 111 EUR` so the account will get 111 Euros. The command creates
a transfer that is sent to the account with 111 EUR. In order to get details about working
of the command see `./fifi/management/commands/load_money.py`.

Check the loaded money in `Transfers` section in the administration panel. So there should only one
transfer with 111 EUR.

### Working as a scheme

As a scheme you are able only authorize a transaction and present a transaction. Ensure you have
`curl` to send POST requests to the service. Actually a scheme don't have own user on the
application because I doubt how scheme interact an issuer. It could be white list of ip addresses
or some sort of authorization so I didn't work on roles and permissions but for production it is
necessary. In our case use `admin` user to perform the requests via curl.

Authorization of a transaction for account has Id `CARD0000` looks like this:

```bash
curl --user admin:admin -H "Content-Type: application/json" -X POST --data '
    {
        "type": "authorisation",
        "card_id": "CARD0000",
        "transaction_id": "1234ZORRO",
        "merchant_name": "SNEAKERS R US",
        "merchant_city": "LOS ANGELES",
        "merchant_country": "US",
        "merchant_mcc": "5139",
        "billing_amount": "90.00",
        "billing_currency": "EUR",
        "transaction_amount": "100.00",
        "transaction_currency": "USD"
    }
' http://localhost:8000/scheme-api/transactions/
```
the request is processed by `fifi.scheme_api.transactions.CreateAPIView`. The view of the
model process the request using `_AuthorizeSerializer` and creates a new transaction and
related transfer that holds 90.00 Euros on the account. If the account doesn't have enough
money the view return response with HTTP 403.

In order present the transaction use curl again. Appropriate command looks like this:

```bash
curl --user admin:admin -H "Content-Type: application/json" -X POST --data '
    {
        "type": "presentment",
        "card_id": "CARD0000",
        "transaction_id": "1234ZORRO",
        "merchant_name": "SNEAKERS R US",
        "merchant_city": "LOS ANGELES",
        "merchant_country": "US",
        "merchant_mcc": "5139",
        "billing_amount": "91.00",
        "billing_currency": "EUR",
        "transaction_amount": "100.00",
        "transaction_currency": "USD",
        "settlement_amount": "90.50",
        "settlement_currency": "EUR"
    }
' http://localhost:8000/scheme-api/transactions/
```
the request is processed by the same view but using the request using `_PresentSerializer`.
It finds a transaction by `transaction_id` and updates it changing billing amount and state,
adding settlement amount. During the updating it changes amount of the transfer that holds
money from 90 EUR to 91 EUR, creates transfer paying the Issuer revenue. The revenue is
equal 0.5 EUR.

The view uses two serializers due to separating logic of validation and logic of post-processing.
Validation of presentment serializer could be more complex, I don't check account, merchant data,
currencies but for production I would prefer check them all. It's not clear whether a scheme use
multiple endpoints so I decided that it can't and I separated the logic onto two serializers.

### Working as a cardholder

The solution doesn't provide any usable UI so there is only UI to perform some API requests.
If you are logged in as admin user then log out and log in as a cardholder. In order to do it
go to `http://localhost:8000/api-auth/login/?next=/api/` and type a username and a password
the cardholder has. The username and the password are created by you during creation of the
cardholder. It was the first your action in the use case.

Now when you are logged in as a cardholder go to `http://localhost:8000/api/transactions/`.
The endpoint shows presented transactions of all accounts of the cardholder. The endpoint
is provided by `fifi.api.transactions.ViewSet`. The endpoint supports filtering by
timeframe of creation date. The filtering is provided by `fifi.api.transactions._FilterSet`

In order to get balances of all accounts of the cardholder go to
`http://localhost:8000/api/accounts/`. The endpoint is provided by `fifi.api.accounts.ViewSet`

### Working as an admin again

The one thing remains is getting revenue the Issuer got. Log out as a cardholder and log in as an
admin again. Go to `http://localhost:8000/admin/`, open Transfers section, find a transfer that has
a title that has the Issuer account name as a part. Like BANK\*\*\*\*. If you performed a
transaction that has billing currency EUR the desired transfer should contain BANK0000 in
its title. If you presented transaction using data from example above then the transfer you find
must have title `0.5 EUR BANK0000 EUR`

Fin.
