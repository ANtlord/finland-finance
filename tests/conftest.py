import pytest

from rest_framework.test import APIClient

from .factories import UserFactory


@pytest.fixture
def user_api_client() -> APIClient:
    user = UserFactory()
    api_client = APIClient()
    api_client.force_authenticate(user)
    api_client.user = user
    return api_client
