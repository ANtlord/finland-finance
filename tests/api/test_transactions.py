from datetime import datetime
from datetime import timedelta
from itertools import chain
from random import randint
from random import random
from typing import List

import pytest
from rest_framework import status
from rest_framework.test import APIClient

from django.utils import timezone
from django.urls import reverse_lazy

from fifi.models import Account, Transaction
from tests.factories import TransactionFactory
from tests.factories import AccountFactory

_TRANSACTION_LIST = reverse_lazy('api:transaction-list')
_TIMEFRAME_IN_DAYS = 5


@pytest.fixture
def user_accounts(user_api_client: APIClient):
    return AccountFactory.create_batch(3, user=user_api_client.user)


@pytest.fixture
def user_transactions(user_accounts: List[Account]):
    factory = TransactionFactory.create_batch
    return [t for a in user_accounts for t in factory(randint(1, 3), account=a)]


def get_transaction_list(client: APIClient, **kwargs):
    response = client.get(_TRANSACTION_LIST, kwargs)
    data = response.data
    assert response.status_code == status.HTTP_200_OK, data
    return data


@pytest.mark.django_db
def test_granted_presented_transactions(user_api_client: APIClient):
    user_transactions = TransactionFactory.create_batch(
        randint(1, 5), state=Transaction.PRESENTMENT, user=user_api_client.user
    )
    data = get_transaction_list(user_api_client)
    assert data['count'] == len(user_transactions)
    assert {x.pk for x in user_transactions} == {x['id'] for x in data['results']}


@pytest.mark.django_db
def test_denied_transactions(user_api_client: APIClient):
    vendor_transactions = TransactionFactory.create_batch(randint(1, 5))
    user_authorized_transactions = TransactionFactory.create_batch(
        randint(1, 5), user=user_api_client.user, state=Transaction.AUTHORIZATION
    )
    unexpected_transactions = chain(vendor_transactions, user_authorized_transactions)
    data = get_transaction_list(user_api_client)
    assert data['count'] == 0
    assert all(t.pk not in {x['id'] for x in data['results']} for t in unexpected_transactions)


def _random_datetime(since, to) -> datetime:
    delta_seconds = (to - since).total_seconds()
    random_margin = delta_seconds * random()
    return since + timedelta(seconds=random_margin)


@pytest.mark.django_db
def test_timeframed_transactions(user_api_client: APIClient):
    now = timezone.now()
    since = now - timedelta(days=_TIMEFRAME_IN_DAYS)
    long_time_ago = since - timedelta(days=randint(1, 5))
    expected_transactions = TransactionFactory.create_batch(
        5,
        user=user_api_client.user,
        created=lambda: _random_datetime(since, now),
        state=Transaction.PRESENTMENT,
    )
    unexpected_transactions = TransactionFactory.create_batch(
        5,
        user=user_api_client.user,
        created=lambda: _random_datetime(long_time_ago, since),
        state=Transaction.PRESENTMENT,
    )
    data = get_transaction_list(
        user_api_client,
        # django_filters doesn't accept timezone, because its field
        # uses Django's DateTimeField instead of DRF's DateTimeField
        # so I need to remove timezone info.
        created_after=since.replace(tzinfo=None),
        created_before=now.replace(tzinfo=None),
    )
    assert data['count'] == len(expected_transactions)
    assert {x.pk for x in expected_transactions} == {x['id'] for x in data['results']}
    assert all(t.pk not in {x['id'] for x in data['results']} for t in unexpected_transactions)
