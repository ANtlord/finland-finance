from itertools import chain
from random import randint

import pytest
from rest_framework.test import APIClient
from rest_framework import status

from django.urls import reverse_lazy

from fifi import models
from tests.factories import AccountFactory
from tests.factories import TransactionFactory
from tests.factories import TransactionTransferFactory
from tests.factories import CashTransferFactory


_PRESENTMENT = models.Transaction.PRESENTMENT
_BALANCE_LIST = reverse_lazy('api:account-list')


def _get_balance_list(client: APIClient, **kwargs):
    response = client.get(_BALANCE_LIST, kwargs)
    data = response.data
    assert response.status_code == status.HTTP_200_OK, data
    return data


@pytest.fixture
def accounts(user_api_client: APIClient):
    return accounts


def _check_balances(user_api_client: APIClient, field_name: str, expected_balances):
    accounts = user_api_client.user.accounts.all()
    data = _get_balance_list(user_api_client)
    assert data['count'] == len(accounts)
    assert {x.pk for x in accounts} == {x['id'] for x in data['results']}
    for result in data['results']:
        assert result[field_name] == expected_balances[result['id']]


@pytest.mark.parametrize('fields', (
    dict(kind=models.Transfer.TRANSACTION, transaction__state=_PRESENTMENT),
    dict(kind=models.Transfer.TRANSACTION),
), ids=['ledger_balance', 'available_balance'])
@pytest.mark.django_db
def test_balances(fields, user_api_client: APIClient):
    accounts = AccountFactory.create_batch(randint(1, 5), user=user_api_client.user)

    transactions = TransactionFactory.create_batch(20, accounts=accounts, state=_PRESENTMENT)
    for t in transactions:
        TransactionTransferFactory(transaction=t)
    CashTransferFactory.create_batch(20, accounts=accounts)

    expected_balances = {}
    for account in accounts:
        account_transaction_transfers = account.transfers.filter(**fields)
        account_cash_transfers = account.transfers.filter(kind=models.Transfer.CASH)
        expected_balances[account.pk] = sum(
            x.amount for x in chain(account_transaction_transfers, account_cash_transfers)
        )
    _check_balances(user_api_client, 'ledger_balance', expected_balances)
