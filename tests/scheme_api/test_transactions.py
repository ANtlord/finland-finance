from factory import fuzzy
import faker
from decimal import Decimal
import pytest
from random import normalvariate

from rest_framework.test import APIClient
from rest_framework import status

from django.conf import settings
from django.urls import reverse_lazy

from fifi import models
from fifi import fields
from tests.factories import AccountFactory
from tests.factories import CashTransferFactory
from tests.factories import TransactionFactory
from tests.factories import TransactionTransferFactory
from tests.factories import UserFactory


_TRANSACTION_STATES_DICT = {x: y for x, y in models.Transaction.STATES}
_TRANSACTION_LIST = reverse_lazy('scheme_api:transaction-list')


def _generate_fuzzy_decimal(for_decimal, variation):
    ret = for_decimal * Decimal(f'{normalvariate(1, variation):.2f}')
    ret = round(ret, 2)
    return ret


def _build_data(account, type_=None):
    fake = faker.Faker()

    transaction_id = '1234ZORRO'
    transaction_currency, _ = fuzzy.FuzzyChoice(choices=fields.Currency.CURRENCIES).fuzz()
    transaction_amount = fuzzy.FuzzyDecimal(low=10, high=1000).fuzz()

    if account.currency == transaction_currency:
        billing_amount = transaction_amount
    else:
        billing_amount = _generate_fuzzy_decimal(transaction_amount, 0.3)

    data = {
        'type': type_,
        'card_id': account.card_id,
        'transaction_id': transaction_id,
        'merchant_name': fake.company(),
        'merchant_country': fake.country_code(),
        'merchant_city': fake.city(),
        'merchant_mcc': fuzzy.FuzzyInteger(1000, 9999).fuzz(),
        'billing_amount': billing_amount,
        'billing_currency': account.currency[0],
        'transaction_amount': transaction_amount,
        'transaction_currency': transaction_currency,
    }
    return data


@pytest.fixture
def account_api_client(user_api_client: APIClient):
    user_api_client.account = AccountFactory(user=user_api_client.user)
    return user_api_client


@pytest.fixture
def authorization_data(account_api_client: APIClient):
    type_ = str(_TRANSACTION_STATES_DICT[models.Transaction.AUTHORIZATION])
    return _build_data(account_api_client.account, type_)


@pytest.fixture
def presentment_data(account_api_client: APIClient):
    transaction = TransactionFactory(
        account=account_api_client.account, state=models.Transaction.AUTHORIZATION
    )
    TransactionTransferFactory(transaction=transaction)
    type_ = str(_TRANSACTION_STATES_DICT[models.Transaction.PRESENTMENT])
    data = _build_data(account_api_client.account, type_)
    data.update({
        'transaction_id': transaction.pk,
        'settlement_amount': _generate_fuzzy_decimal(transaction.billing_amount, 0.05),
        'settlement_currency': transaction.billing_currency,
    })
    return data


def _check_transaction(user_api_client, data):
    transaction_id = data['transaction_id']
    response = user_api_client.post(_TRANSACTION_LIST, data)
    assert response.status_code == status.HTTP_200_OK, response.data
    transaction = models.Transaction.objects.get(id=transaction_id)
    return transaction


@pytest.mark.django_db
def test_success_authorization(account_api_client: APIClient, authorization_data: dict):
    CashTransferFactory(
        account=account_api_client.account,
        amount=authorization_data['billing_amount'] + 1
    )
    transaction = _check_transaction(account_api_client, authorization_data)
    transfer = models.TransactionTransfer.objects.get(transaction_id=transaction.pk)
    assert transfer.amount == -transaction.billing_amount
    assert transaction.is_authorized


@pytest.mark.django_db
def test_fail_authorization(account_api_client: APIClient, authorization_data: dict):
    transaction_id = authorization_data['transaction_id']
    response = account_api_client.post(_TRANSACTION_LIST, authorization_data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.data
    assert not models.Transaction.objects.filter(id=transaction_id).exists()


@pytest.mark.django_db
def test_success_presentment(account_api_client: APIClient, presentment_data: dict):
    issuer_user = UserFactory(username=settings.ISSUER_USER)
    AccountFactory(user=issuer_user, currency=account_api_client.account.currency)
    transaction = _check_transaction(account_api_client, presentment_data)

    models.Transaction.objects.count() == 1
    issuer_revenue_transfer = models.TransactionTransfer.objects.get(
        transaction_id=transaction.pk,
        account__user_id=issuer_user.pk,
    )
    billing_transfer = models.TransactionTransfer.objects.get(
        transaction_id=transaction.pk, account_id=transaction.account_id,
    )
    assert billing_transfer.amount == -transaction.billing_amount
    assert issuer_revenue_transfer.amount == (
        transaction.billing_amount - transaction.settlement_amount
    )
    assert transaction.is_presented
    assert transaction.settlement_amount == presentment_data['settlement_amount']


@pytest.mark.parametrize('wrong_value', (
    {'transaction_id': 'non_exist_transaction_id'},
    {'settlement_amount': fuzzy.FuzzyDecimal(low=-100, high=-1).fuzz()},
), ids=['wrong transaction_id', 'wrong settlement_amount'])
@pytest.mark.django_db
def test_fail_presentment(wrong_value, account_api_client: APIClient, presentment_data: dict):
    presentment_data.update(wrong_value)
    response = account_api_client.post(_TRANSACTION_LIST, presentment_data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.data
