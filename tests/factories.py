from typing import Callable
from typing import TypeVar
from decimal import Decimal
import factory
from datetime import datetime
from random import normalvariate
from random import choice
from factory import fuzzy

from django.contrib.auth import get_user_model

from fifi import models
from fifi import fields

User = get_user_model()


_TRANSACTION_STATES = models.Transaction.STATES
_INNACURACY_RATE = Decimal('0.05')


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Sequence(lambda n: f'user{n:02d}')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')

    class Meta:
        model = User


class AccountFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    id = factory.Sequence(lambda n: f'CARD{n:04d}')
    currency = fuzzy.FuzzyChoice(choices=fields.Currency.CURRENCIES)

    class Meta:
        model = models.Account


DT = TypeVar('DT', datetime, Callable[[], datetime])


class TransactionFactory(factory.django.DjangoModelFactory):
    id = factory.Sequence(lambda n: f'{n:04d}TRANS')
    account = factory.LazyAttribute(
        lambda o: AccountFactory(user=o.user) if o.user else AccountFactory()
    )
    amount = fuzzy.FuzzyDecimal(low=10, high=1000)
    currency = fuzzy.FuzzyChoice(choices=fields.Currency.CURRENCIES)

    billing_amount = factory.LazyAttribute(
        lambda o: o.amount if o.account.currency == o.currency
        else round(o.amount * Decimal(f'{normalvariate(1, 0.2):.2f}'), 2)
    )
    state = fuzzy.FuzzyChoice(choices=_TRANSACTION_STATES)
    merchant_name = factory.Faker('company')
    merchant_country = factory.Faker('country_code')
    merchant_city = factory.Faker('city')
    merchant_mcc = factory.Sequence(lambda n: f'{n:04d}')

    @factory.lazy_attribute
    def settlement_amount(self):
        if self.state == models.Transaction.PRESENTMENT:
            val = normalvariate(
                float(self.billing_amount),
                float(self.billing_amount * _INNACURACY_RATE)
            )
            return Decimal(f'{val:.2f}')
        else:
            return None

    class Meta:
        model = models.Transaction

    class Params:
        user = None
        accounts = None

    @factory.post_generation
    def created(self, is_created: bool, extracted: DT, **kwargs):
        if not is_created:
            return
        if extracted is not None:
            if isinstance(extracted, datetime):
                self.created = extracted
            elif callable(extracted):
                self.created = extracted()
            else:
                raise TypeError(f'extracted = {repr(extracted)} unsupperted type')

    @factory.lazy_attribute
    def account(self):
        if self.user:
            return AccountFactory(user=self.user)
        elif self.accounts:
            return choice(self.accounts)
        else:
            return AccountFactory()


class TransactionTransferFactory(factory.django.DjangoModelFactory):
    amount = factory.LazyAttribute(lambda o: -o.transaction.billing_amount)
    account = factory.LazyAttribute(lambda o: o.transaction.account)

    class Meta:
        model = models.TransactionTransfer

    class Params:
        user = None
        accounts = None

    @factory.lazy_attribute
    def transaction(self):
        if self.user:
            return TransactionFactory(user=self.user)
        else:
            return TransactionFactory()


class CashTransferFactory(factory.django.DjangoModelFactory):
    amount = fuzzy.FuzzyDecimal(low=10, high=1000)
    transaction = None

    class Meta:
        model = models.CashTransfer

    class Params:
        user = None
        accounts = None

    @factory.lazy_attribute
    def account(self):
        if self.user:
            return AccountFactory(user=self.user)
        elif self.accounts:
            return choice(self.accounts)
        else:
            return AccountFactory()
